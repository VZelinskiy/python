import sys
import unicurses
from unicurses import *


board = [' ',' ',' ',' ',' ',' ',' ',' ',' ']

def symbols():
	print("Enter x or o")
	try:
		player_symbol = input()
	except:
	 	player_symbol = raw_input()
	if player_symbol == "x" or player_symbol == "X":
		computer_symbol = "O"
		player_symbol = "X"
	else:
		computer_symbol = "X"
		player_symbol = "O"
	symbol = [player_symbol, computer_symbol]
	return symbol
def draw(board):
	stdscr = initscr()
	noecho()
	cbreak()
	mvaddstr(1, 1, '-------')

	for i in range(3):
		mvaddstr(2+i*2, 1, "|")
		mvaddstr(2+i*2, 2, board[3*i])
		mvaddstr(2+i*2, 3, "|")
		mvaddstr(2+i*2, 4, board[3*i+1])
		mvaddstr(2+i*2, 5, "|")
		mvaddstr(2+i*2, 6, board[3*i+2])
		mvaddstr(2+i*2, 7, "|")
		mvaddstr(3+i*2, 1, '-------')

	refresh()
	getch()
	endwin()
    
def player(symbol):
	print("Enter coord")
	check = False
	while not check:
		try:
			position = input()
		except:
	 		position = raw_input()
		if int(position) >= 1 and int(position) <= 9:
			if board[int(position)-1] == ' ':
				board[int(position)-1] = symbol[0]
				check = True
			else:
				print("Please, try another place")
def computer(symbol):
	done1 = step(1, symbol)
	done2 = ''
	if done1 == False: 
		done2 = step(0, symbol)
	if done2 == False:
		for i in range(9):
			if board[i] == ' ':
	 			board[i] = symbol[1]
	 			break

def step(par, symbol):
	res=False
	counter = 0
	for i in range(9):
		if board[i]==' ': counter+=1
	for i in range(9):
		if board[4]==' ': 
			board[4]=symbol[par]
			break
		if i<8 and board[i]==symbol[par] and board[i+1]==symbol[par]:
			if i%3 != 2:
				if i%3 == 0:
					if board[i+2]==' ': 
						board[i+2] = symbol[1]
						break	
				elif board[i-1]==' ': 
					board[i-1] = symbol[1]
					break				
		if i<7 and board[i]==symbol[par] and board[i+2]==symbol[par]:
			if i%3 == 0:
				if board[i+1]==' ': 
					board[i+1] = symbol[1]
					break					
		if i<6 and board[i]==symbol[par] and board[i+3]==symbol[par]:
			if i<3:
				if board[i+6]==' ': 
					board[i+6] = symbol[1]
					break				
			elif board[i-3]==' ': 
				board[i-3] = symbol[1]
				break		
		if i<3 and board[i]==symbol[par] and board[i+6]==symbol[par]:
			if board[i+3]==' ': 
				board[i+3] = symbol[1]
				break


		if i<5 and board[i]==symbol[par] and board[i+4]==symbol[par]:
			if i==0 and board[8]==' ': 
				board[8]=symbol[1]
				break
			if i==4 and board[0]==' ': 
				board[0]=symbol[1]
				break				
		if i<5 and board[i]==symbol[par] and board[i+2]==symbol[par]:
			if i==2 and board[6]==' ': 
				board[6]=symbol[1]
				break				
			if i==4 and board[2]==' ': 
				board[2]=symbol[1]
				break
		if board[0]==symbol[par] and board[8]==symbol[par] or board[2]==symbol[par] and board[6]==symbol[par]:
			if board[4]==' ': 
				board[4]=symbol[1]
				break
	counter2 = 0
	for i in range(9):
		if board[i]==' ': counter2+=1
	if counter!=counter2: res=True
	return res

def check_win(board):
    win_variants = ((0,1,2),(3,4,5),(6,7,8),(0,3,6),(1,4,7),(2,5,8),(0,4,8),(2,4,6))
    for i in win_variants:
        if board[i[0]] == board[i[1]] == board[i[2]] and board[i[0]]!=' ':
            return board[i[0]]
    for i in range(9):
    	if board[i]==' ':
    		return False
    return "none"
def main(board):
	symbol = symbols()
	end = False
	while not end:
		player(symbol)
		computer(symbol)
		draw(board)
		res = check_win(board)
		if res!=False: 
			if res == symbol[0]:
				print("Player win!")
				end = True
				break
			if res == "none":
				print("Nobody wins")
			else: print("Computer win")
			end = True
main(board)    
